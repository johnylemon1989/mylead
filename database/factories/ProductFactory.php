<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Product>
 */
class ProductFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition(): array
    {
        return [
            'name' => implode(' ', fake()->words(rand(2,5))),
            'description' => implode(PHP_EOL, fake()->paragraphs(rand(1,5))),
        ];
    }

    /**
     * Configure the model factory.
     */
    public function configure(): static
    {
        return $this->afterCreating(function ($product) {

            \App\Models\Price::factory()->count(rand(1, 5))->create([
                'product_id' => $product->id
            ]);
        });
    }
}
