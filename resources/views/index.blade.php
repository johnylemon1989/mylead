@extends('layout')

@section('content')

{{-- this is intentionsl -> no need to fight with css ansd so on for backend dev position --}}

<style>
    svg {
        display: none;
    }
    form {
        display: grid;
        grid-gap: 1em;
        grid-template-columns: repeat(4, 1fr)
    }

    table {
        width: 100%;
        border: black 1px solid
    }

    table td {
        border-top: black 1px solid
    }

    table th {
        text-align: left
    }

</style>

<div>
    @auth
        <a href="{{ route('products.create') }}">create</a>
        <form action="{{ route('logout') }}" method="post">
            {{ csrf_field() }}
            <button>logout</button>
        </form>
    @endauth
    @guest
        <a href="{{ route('login') }}">login</a>
    @endguest
</div>
<h2>Products listing</h2>


<form>
    <div>
        <div>
            filter by name
        </div>
        <input type="text" name="name" placeholder="filter by name" value="{{ request()->name }}" />
    </div>
    <div>
        <div>
            sorting order
        </div>
        <select name="direction">
            @foreach(\App\Enums\SortingOrder::cases() as $case)
                <option value="{{ $case->value }}" {{ request()->direction === $case->value ? 'selected' : '' }}>name {{ $case->value }}</option>
            @endforeach
        </select>
    </div>

    <button>apply</button>
</form>

<table>
    <thead>
        <tr>
            <th>id</th>
            <th>name</th>
            <th>prices count</th>
            <th></th>
        </tr>
    </thead>
    <tbody>
        @foreach($products as $product)
        <tr>
            <td>{{ $product->id }}</td>
            <td>{{ $product->name }}</td>
            <td>{{ $product->prices_count }}</td>
            <td>
                <a href="{{ route('show', $product) }}">show</a>

                @auth
                    <a href="{{ route('products.edit', $product) }}">edit</a>

                    <form action="{{ route('products.delete', $product) }}">
                        {{ method_field('DELETE')}}
                        <button>delete</button>
                    </form>
                @endauth

            </td>
        </tr>
        @endforeach
    </tbody>
</table>

{!! $products->links() !!}

@endsection

