@extends('admin.layout')

@section('content')

<h2>Edit product {{ $product->id }}</h2>

<form method="POST" action="{{ route('products.update', $product) }}">
    {{ csrf_field() }}
    {{ method_field('PATCH') }}

    <h4>name</h4>
    <div>
        <input type="text" name="name" placeholder="name" value="{{ $product->name }}" />
    </div>
    <h4>description</h4>
    <div>
        <textarea name="description">{{ $product->description }}</textarea>
    </div>

    <h4>prices</h4>

    <ul>
        @foreach($product->prices as $price)
            <li>
                {{ $price->valueFormatted() }}

                @auth
                <form method="POST" action="{{ route('products.prices.delete', [$product, $price]) }}">
                    {{ csrf_field() }}
                    {{ method_field('DELETE') }}
                    <button>delete</button>
                </form>
                @endauth
            </li>
        @endforeach

    </ul>

    <h4>add price</h4>
    <form action="{{ route('products.prices.store', $product) }}" method="POST">
        {{ csrf_field() }}
        <input type="text" name="value" placeholder="value in cents" />
    </form>
    <button>submit</button>
</form>


@endsection

