@extends('admin.layout')

@section('content')

<h2>Create product</h2>

<form method="POST" action="{{ route('products.store') }}">
    {{ csrf_field() }}

    <h4>name</h4>
    <div>
        <input type="text" name="name" placeholder="name" />
    </div>
    <h4>description</h4>
    <div>
        <textarea name="description"></textarea>
    </div>

    <h4>prices (space separated)</h4>
    <div>
        <input type="text" name="prices" placeholder="eg: 10.00, 0.99" />
    </div>
    <button>submit</button>
</form>


@endsection

