@extends('layout')

@section('content')

<div>
    <a href="{{ route('index') }}">back to listinig</a>
    @auth
        <a href="{{ route('products.edit', $product) }}">edit</a>
    @endauth
</div>

<h2>{{ $product->name }}</h2>
<p>{!! nl2br($product->description) !!}</p>


<h4>Price variants</h4>
<ul>
    @foreach($product->prices as $price)
        <li>{{ $price->valueFormatted() }}</li>
    @endforeach
</ul>

@endsection
