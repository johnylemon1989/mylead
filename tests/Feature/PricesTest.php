<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

use App\Models\User;
use App\Models\Product;
use App\Models\Price;
use DB;

class PricesTest extends TestCase
{
    use RefreshDatabase;

    protected User $user;
    protected Product $p1;
    protected Product $p2;

    public function setUp(): void
    {
        parent::setUp();

        $this->user = User::factory()->create([]);
        $this->p1 = Product::factory()->create([]);
        $this->p2 = Product::factory()->create([]);

    }

    public function test_cannot_add_price_if_not_logged(): void
    {
        $pricesCount = DB::table('prices')->count();
        $response = $this->post(route('products.prices.store', $this->p1), [
            'value' => 100,
        ]);

        $response->assertStatus(302);
        $response->assertLocation(route('login'));
        $this->assertDatabaseCount('prices', $pricesCount);
    }

    public function test_can_add_price_if_logged(): void
    {
        $pricesCount = DB::table('prices')->count();
        $response = $this->actingAs($this->user)->post(route('products.prices.store', $this->p1), [
            'value' => 100,
        ]);

        $this->assertDatabaseCount('prices', $pricesCount+1);
    }

    public function test_price_is_attached_to_correct_product(): void
    {
        $p1Count = $this->p1->prices()->count();
        $p2Count = $this->p2->prices()->count();

        $response = $this->actingAs($this->user)->post(route('products.prices.store', $this->p2), [
            'value' => 100,
        ]);

        $this->assertEquals($p1Count, $this->p1->prices()->count());
        $this->assertEquals($p2Count + 1, $this->p2->prices()->count());

        $this->assertDatabaseCount('prices', $p1Count + $p2Count + 1);
    }

    public function test_cannot_delete_price_if_not_logged(): void
    {
        $price = $this->p1->prices()->create(['value' => 100]);
        $pricesCount = DB::table('prices')->count();

        $response = $this->delete(route('products.prices.delete', [$this->p1, $price]));
        $response->assertStatus(302);
        $response->assertLocation(route('login'));

        $this->assertDatabaseCount('prices', $pricesCount);
    }

    public function test_can_delete_price_if_logged(): void
    {
        $price = $this->p1->prices()->create(['value' => 100]);
        $pricesCount = DB::table('prices')->count();

        $response = $this->actingAs($this->user)->delete(route('products.prices.delete', [$this->p1, $price]));

        $this->assertDatabaseCount('prices', $pricesCount - 1);
    }

    public function test_deletes_price_from_correct_product(): void
    {
        $price1 = $this->p1->prices()->create(['value' => 100]);
        $price2 = $this->p2->prices()->create(['value' => 100]);
        $p1Cnt = $this->p1->prices()->count();
        $p2Cnt = $this->p2->prices()->count();

        $response = $this->actingAs($this->user)->delete(route('products.prices.delete', [$this->p2, $price2]));

        $this->assertEquals($p1Cnt, $this->p1->prices()->count());
        $this->assertEquals($p2Cnt - 1, $this->p2->prices()->count());

        $this->assertDatabaseCount('prices', $p1Cnt + $p2Cnt - 1);
    }

    public function test_cannot_delete_price_from_incorrect_product(): void
    {
        $price1 = $this->p1->prices()->create(['value' => 100]);
        $price2 = $this->p2->prices()->create(['value' => 100]);
        $p1Cnt = $this->p1->prices()->count();
        $p2Cnt = $this->p2->prices()->count();

        $response = $this->actingAs($this->user)->delete(route('products.prices.delete', [$this->p2, $price1]));

        $this->assertEquals($p1Cnt, $this->p1->prices()->count());
        $this->assertEquals($p2Cnt, $this->p2->prices()->count());

        $this->assertDatabaseCount('prices', $p1Cnt + $p2Cnt);
    }
}
