<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

use App\Models\User;
use App\Models\Product;
use App\Services\Listings\ProductListing;

class FiltersTest extends TestCase
{
    use RefreshDatabase;

    protected Product $p1;
    protected Product $p2;
    protected Product $p3;
    protected Product $p4;

    public function setUp(): void
    {
        parent::setUp();

        $this->p1 = Product::factory()->create(['name' => 'a']);
        $this->p2 = Product::factory()->create(['name' => 'c']);
        $this->p3 = Product::factory()->create(['name' => 'b']);
        $this->p4 = Product::factory()->create(['name' => 'bb']);
    }

    public function test_no_filters_applied()
    {
        $listing = new ProductListing([], Product::query());
        $this->assertEquals('a c b bb', $listing->pluck('name')->join(' '));
    }

    public function test_filtered_by_name()
    {
        $listing = new ProductListing(['name' => 'b'], Product::query());
        $this->assertEquals('b bb', $listing->pluck('name')->join(' '));

        $listing = new ProductListing(['name' => 'x'], Product::query());
        $this->assertEquals('', $listing->pluck('name')->join(' '));
    }

    public function test_ordered_asc()
    {
        $listing = new ProductListing(['direction' => 'asc'], Product::query());
        $this->assertEquals('a b bb c', $listing->pluck('name')->join(' '));
    }

    public function test_ordered_desc()
    {
        $listing = new ProductListing(['direction' => 'desc'], Product::query());
        $this->assertEquals('c bb b a', $listing->pluck('name')->join(' '));
    }

    public function test_ordered_none()
    {
        $listing = new ProductListing(['direction' => 'xxxx'], Product::query());
        $this->assertEquals('a c b bb', $listing->pluck('name')->join(' '));
    }
}
