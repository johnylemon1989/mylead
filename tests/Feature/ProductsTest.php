<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

use App\Models\User;
use App\Models\Product;

class ProductsTest extends TestCase
{
    use RefreshDatabase;

    protected User $user;

    public function setUp(): void
    {
        parent::setUp();

        $this->user = User::factory()->create([]);
    }


    public function test_cannot_create_product_if_not_loggedin(): void
    {
        $response = $this->post(route('products.store'), [
            'name' => 'aaa',
            'description' => 'description',
        ]);

        $response->assertStatus(302);
        $response->assertLocation(route('login'));
        $this->assertDatabaseCount('products', 0);
    }

    public function test_cannot_edit_product_if_not_loggedin(): void
    {
        $product = Product::factory()->create([]);

        $response = $this->get(route('products.edit', $product));
        $response->assertStatus(302);
        $response->assertLocation(route('login'));


        $response = $this->patch(route('products.update', $product), [
            'name' => 'aaa',
            'description' => 'description',
        ]);

        $response->assertStatus(302);
        $response->assertLocation(route('login'));
        $this->assertDatabaseCount('products', 1);
    }

    public function test_cannot_delete_product_if_not_loggedin(): void
    {
        $product = Product::factory()->create([]);
        $this->assertDatabaseCount('products', 1);

        $response = $this->delete(route('products.delete', $product));
        $response->assertStatus(302);

        $this->assertDatabaseCount('products', 1);
    }

    public function test_can_delete_product_if_loggedin(): void
    {
        $product = Product::factory()->create([]);
        $this->assertDatabaseCount('products', 1);

        $response = $this->actingAs($this->user)->delete(route('products.delete', $product));
        $response->assertLocation(route('index'));

        $this->assertDatabaseCount('products', 0);
    }

    public function test_can_create_product_if_loggedin(): void
    {
        $this->assertDatabaseCount('products', 0);

        $response = $this->actingAs($this->user)->post(route('products.store'), [
            'name' => 'aaa',
            'description' => 'description',
        ]);

        $this->assertDatabaseCount('products', 1);
    }

    public function test_create_product_with_invalid_data(): void
    {
        $response = $this->actingAs($this->user)->post(route('products.store'), [
            'name' => '',
            'description' => '',
        ]);

        $response->assertStatus(302);

        $this->assertDatabaseCount('products', 0);
    }


    public function test_update_product_with_invalid_data(): void
    {
        $product = Product::factory()->create([]);

        $response = $this->actingAs($this->user)->patch(route('products.update', $product), [
            'name' => '',
            'description' => '',
        ]);

        $response->assertStatus(302);

        $this->assertDatabaseCount('products', 1);
    }

}
