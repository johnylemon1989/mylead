<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Http\Requests\StoreProductsRequest;
use App\Http\Requests\UpdateProductsRequest;

use App\Models\Product;

class ProductController extends Controller
{
    public function create(Request $request)
    {
        return view('admin.create');
    }

    public function store(StoreProductsRequest $request)
    {
        $product = Product::create($request->only([
            'name',
            'description',
        ]));

        return redirect()->route('products.edit', $product);
    }

    public function edit(Request $request, Product $product)
    {
        return view('admin.edit', [
            'product' => $product
        ]);
    }

    public function update(UpdateProductsRequest $request, Product $product)
    {
        $product->update($request->only([
            'name',
            'description',
        ]));

        return redirect()->route('products.edit', $product);
    }

    public function destroy(Request $request, Product $product)
    {
        $product->delete();

        return redirect()->route('index');
    }
}
