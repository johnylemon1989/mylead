<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Http\Requests\StorePriceRequest;
use App\Models\Product;
use App\Models\Price;

class PriceController extends Controller
{
    public function store(StorePriceRequest $request, Product $product)
    {
        $price = $product->prices()->create($request->only([
            'value',
        ]));

        return redirect()->route('products.edit', $product);
    }

    public function destroy(Request $request, Product $product, Price $price)
    {
        $product->prices()->where('id', $price->id)->delete();

        return redirect()->route('products.edit', $product);
    }
}
