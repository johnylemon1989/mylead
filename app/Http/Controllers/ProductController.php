<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Models\Product;
use App\Services\Listings\ProductListing;

class ProductController extends Controller
{
    public function index(Request $request)
    {
        $listing = new ProductListing($request->all(), Product::withCount('prices'));

        return view('index', [
            'products' => $listing->paginate(),
        ]);
    }

    public function show(Request $request, Product $product)
    {
        return view('show', [
            'product' => $product
        ]);
    }
}
