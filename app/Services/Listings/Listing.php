<?php

namespace App\Services\Listings;

abstract class Listing
{
    public function __construct(protected array $data, protected $query)
    {
        $this->applyFilters();
    }

    protected function applyFilters(): void
    {
        foreach($this->filters() as $filter)
            (new $filter($this->data))->apply($this->query);
    }

    protected function filters(): array
    {
        return [
            //
        ];
    }

    public function __call($name, $params)
    {
        return $this->query->$name(...$params);
    }
}
