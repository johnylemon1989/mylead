<?php

namespace App\Services\Listings;

class ProductListing extends Listing
{
    protected function filters(): array
    {
        return [
            Filters\OrderFilter::class,
            Filters\NameFilter::class,
        ];
    }
}
