<?php

namespace App\Services\Listings\Filters;

class NameFilter extends Filter
{
    public function apply($query): void
    {
        if($name = data_get($this->data, 'name'))
            $query->where('name', 'LIKE', "%$name%");
    }
}

