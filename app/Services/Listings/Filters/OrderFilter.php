<?php

namespace App\Services\Listings\Filters;

use App\Enums\SortingOrder;

class OrderFilter extends Filter
{
    public function apply($query): void
    {
        $direction = SortingOrder::tryFrom(data_get($this->data, 'direction'));

        if($direction?->shouldApply())
            $query->orderBy('name', $direction->value);

    }
}

