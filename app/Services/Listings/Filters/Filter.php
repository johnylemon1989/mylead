<?php

namespace App\Services\Listings\Filters;

abstract class Filter
{
    public function __construct(protected array $data)
    {
        //
    }

    public function apply($query): void
    {
        //
    }
}

