<?php

namespace App\Enums;

enum SortingOrder: string
{
    case NONE = 'default';
    case ASC = 'asc';
    case DESC = 'desc';

    public function shouldApply(): bool
    {
        return $this !== static::NONE;
    }
}
