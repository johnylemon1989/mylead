<?php

use Illuminate\Support\Facades\Route;

use App\Http\Controllers\ProductController;
use App\Http\Controllers\Admin\ProductController as AdminController;
use App\Http\Controllers\Admin\PriceController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

Route::middleware('auth')->prefix('products')->name('products')->group(function(){

    Route::get('create', [AdminController::class, 'create'])->name('.create');
    Route::post('', [AdminController::class, 'store'])->name('.store');
    Route::get('{product}/edit', [AdminController::class, 'edit'])->name('.edit');
    Route::patch('{product}', [AdminController::class, 'update'])->name('.update');
    Route::delete('{product}', [AdminController::class, 'destroy'])->name('.delete');

    Route::prefix('{product}/prices')->name('.prices')->group(function(){

        Route::post('store', [PriceController::class, 'store'])->name('.store');
        Route::delete('{price}', [PriceController::class, 'destroy'])->name('.delete');

    });
});

Route::get('', [ProductController::class, 'index'])->name('index');
Route::get('{product}', [ProductController::class, 'show'])->name('show');
